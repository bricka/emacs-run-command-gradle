;;; run-command-gradle.el --- Interact with Gradle via run-command  -*- lexical-binding: t; -*-

;; Copyright 2023 Alex Figl-Brick

;; Author: Alex Figl-Brick <alex@alexbrick.me>
;; Version: 0.1
;; Package-Requires: ((emacs "29.1"))
;; Homepage: https://gitlab.com/bricka/emacs-run-command-gradle

;;; Commentary:

;; This package provides integration between run-command and Gradle.
;; Unfortunately, because querying Gradle to find out about tasks is
;; incredibly slow, this package instead allows you to define the
;; tasks that you want to run, and is configurable enough to support
;; lots of common use cases.

;; `run-command-gradle-tasks' defines the tasks that you are able to
;; run.  In each of your projects, define
;; `run-command-gradle-projects' as a directory-local variable, which
;; defines the subprojects that you might want to use (or leave empty
;; if you don't have any).

;; A task is an alist with a number of keys:
;; 'task :: The name of the task
;; 'runner (optional) :: A runner as defined by run-command
;; 'project-local-p (optional, default false) :: Whether or not the task is defined per-subproject
;; 'predicate (optional) :: If provided, only provide the task if this predicate returns true
;; arguments (optional) :: Additional arguments to the task
;; name-comment (optional) :: In the list of commands, a comment that will be displayed in brackets
;; wrapper (optional) :: A command that will be prepended to the given Gradle execution

;; Note that use of a wrapper introduces the possibility of unsafe
;; variables. In order to use wrapper with directory-local variables,
;; you need to add approved wrappers to
;; `run-command-gradle-safe-wrapper-commands'.

;;; Code:

(require 'cl-lib)
(require 'dash)
(require 'run-command)

(defvar run-command-gradle-tasks nil
  "The tasks that you want to use with Gradle.

This should be a list of alists with the structure defined in the package
description.")
(put 'run-command-gradle-tasks 'safe-local-variable #'run-command-gradle--safe-task-p)

(defvar run-command-gradle-safe-wrapper-commands nil
  "A list of wrapper commands that are considered safe to run.")
(put 'run-command-gradle-safe-wrapper-commands 'risky-local-variable t)

(defun run-command-gradle--safe-task-p (object)
  "Determine whether OBJECT represents a safe value for Gradle tasks.

Whether a task is unsafe basically boils down to: does it have a
wrapper command that could do damage? Therefore, if there is no
wrapper, it's safe. If there is a wrapper, then we check it
against an allowlist stored in
`run-command-gradle-safe-wrapper-commands'."
  (and
   (listp object)
   (cl-every
    (lambda (task)
      (let-alist task
        (or (null .wrapper)
            (member .wrapper run-command-gradle-safe-wrapper-commands))))
    object)))

(defvar run-command-gradle-projects nil "The subprojects of this Gradle project.")
(make-local-variable 'run-command-gradle-projects)
(put 'run-command-gradle-projects 'safe-local-variable #'list-of-strings-p)

(defun run-command-gradle--task-commands-for-task (task)
  "Determine the task commands for a given TASK."
  (if (not (alist-get 'project-local-p task))
      (list (alist-get 'task task))
    (mapcar
     (lambda (project) (format ":%s:%s" project (alist-get 'task task)))
     run-command-gradle-projects)))

(defun run-command-gradle--commands-for-task (task project-dir gradlew)
  "Return the `run-command' commands based on the given TASK.

The commands will run in PROJECT-DIR, using GRADLEW as the Gradle command."
  (let-alist task
    (when (or (null .predicate) (funcall .predicate))
      (mapcar
       (lambda (cmd)
         (append
          (list :command-name (concat
                               cmd
                               (when .wrapper
                                 (format " (wrapped in `%s`)" .wrapper))
                               (when .comment
                                 (concat " (" .comment ")")))
                :working-dir project-dir
                :command-line (concat (if .wrapper (format "%s " .wrapper) "")
                                      gradlew
                                      " "
                                      cmd
                                      (when .arguments
                                        (concat " " .arguments))))
          (when .runner
            (list :runner .runner))))
       (run-command-gradle--task-commands-for-task task)))))

(defun run-command-recipe-gradle ()
  "Recipes for scripts that use Gradle."
  (when-let* ((project-dir (locate-dominating-file default-directory "gradlew"))
              (maybe-gradlew (concat project-dir "gradlew"))
              (gradlew (when (file-exists-p maybe-gradlew) maybe-gradlew)))
    (-flatten-n 1 (mapcar (lambda (task) (run-command-gradle--commands-for-task task project-dir gradlew)) run-command-gradle-tasks))))

(add-to-list 'run-command-recipes #'run-command-recipe-gradle)

(provide 'run-command-gradle)
;;; run-command-gradle.el ends here
