(require 'run-command-gradle)
(require 'el-mock)

(ert-deftest run-command-gradle--no-gradle-found ()
  "Test that no recipe is produced if Gradle is not found."
  (with-temp-buffer
    (setq-local
     run-command-gradle-tasks (list (make-run-command-gradle-task :task "build")))
    (with-mock
      (mock (locate-dominating-file * "gradlew"))
      (should (eq (run-command-recipe-gradle) nil)))))

(ert-deftest run-command-gradle--single-task ()
  "Test that a single task produces a recipe."
  (with-temp-buffer
    (setq-local run-command-gradle-tasks '(((task . "build"))))
    (with-mock
      (mock (locate-dominating-file * "gradlew") => "/foo/")
      (mock (file-exists-p "/foo/gradlew") => t)
      (should (equal
               (run-command-recipe-gradle)
               (list (list :command-name "build"
                           :working-dir "/foo/"
                           :command-line "/foo/gradlew build")))))))

(ert-deftest run-command-gradle--single-task-project-local ()
  "Test that a single task produces a recipe."
  (with-temp-buffer
    (setq-local run-command-gradle-tasks '(((task . "build")
                                            (project-local-p . t)))
                run-command-gradle-projects '("foo" "bar"))
    (with-mock
      (mock (locate-dominating-file * "gradlew") => "/foo/")
      (mock (file-exists-p "/foo/gradlew") => t)
      (should (equal
               (run-command-recipe-gradle)
               (list (list :command-name ":foo:build"
                           :working-dir "/foo/"
                           :command-line "/foo/gradlew :foo:build")
                     (list :command-name ":bar:build"
                           :working-dir "/foo/"
                           :command-line "/foo/gradlew :bar:build")))))))

(ert-deftest run-command-gradle--wrapper ()
  "Test that a single task produces a recipe."
  (with-temp-buffer
    (setq-local run-command-gradle-tasks '(((task . "build")
                                            (wrapper . "wrapper_cmd"))))
    (with-mock
      (mock (locate-dominating-file * "gradlew") => "/foo/")
      (mock (file-exists-p "/foo/gradlew") => t)
      (should (equal
               (run-command-recipe-gradle)
               (list (list :command-name "build (wrapped in `wrapper_cmd`)"
                           :working-dir "/foo/"
                           :command-line "wrapper_cmd /foo/gradlew build")))))))

(ert-deftest run-command-gradle--safe-with-no-wrapper ()
  "Test that dir-local tasks with no wrappers are safe."
  (with-temp-buffer
    (setq-local run-command-gradle-safe-wrapper-commands nil)
    (should (equal
             (run-command-gradle--safe-task-p '(((task . "foo"))
                                                ((task . "bar"))))
             t))))

(ert-deftest run-command-gradle--safe-with-approved-wrappers ()
  "Test that dir-local tasks with approved wrappers are safe."
  (with-temp-buffer
    (setq-local run-command-gradle-safe-wrapper-commands '("foo" "bar exec"))
    (should (equal
             (run-command-gradle--safe-task-p '(((task . "foo")
                                                 (wrapper . "foo"))
                                                ((task . "bar")
                                                 (wrapper . "bar exec"))))
             t))))

(ert-deftest run-command-gradle--unsafe-with-unapproved-wrappers ()
  "Test that dir-local tasks with an unapproved wrapper are unsafe."
  (with-temp-buffer
    (setq-local run-command-gradle-safe-wrapper-commands '("foo" "bar list"))
    (should (equal
             (run-command-gradle--safe-task-p '(((task . "foo")
                                                 (wrapper . "foo"))
                                                ((task . "bar")
                                                 (wrapper . "bar exec"))))
             nil))))
